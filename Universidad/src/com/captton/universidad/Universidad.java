package com.captton.universidad;
import java.util.ArrayList;


public class Universidad {
	
	private String nombre;
	private String tipo;
	private ArrayList<Alumno> alumnos;
	
	public Universidad(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		alumnos = new ArrayList<Alumno>();
	}
	
	public void inscribirAlumno(Alumno alumn) {
		
		this.alumnos.add(alumn);
		
	}
	
	public void buscarXdni(int dni) {
		
		Boolean encontro = false;
		
		for (Alumno alum : this.alumnos) {
			
			if(alum.getDni() == dni) {
				
				encontro = true;
				System.out.println("Alumno encontrado");
				
			}
			
		}
		
		if(encontro == false) {
			
			System.out.println("No se encontro el alumno con ese DNI");
		}
	}
	

}
