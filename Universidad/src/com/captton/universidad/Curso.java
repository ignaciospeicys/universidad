package com.captton.universidad;

public class Curso {
	
	private int cargaHoraria;
	private int codigo;
	private String nombre;
	private Profesor profesor;
	
	public Curso(int cargaHoraria, int codigo, String nombre) {
		super();
		this.cargaHoraria = cargaHoraria;
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	
	
	

}
