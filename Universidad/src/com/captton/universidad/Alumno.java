package com.captton.universidad;

import java.util.ArrayList;

public class Alumno {
	
	private int dni;
	private String nombre;
	private String apellido;
	private ArrayList<Curso> cursos;
	
	public Alumno(int dni, String nombre, String apellido) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.cursos = new ArrayList<Curso>();
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void inscribirCurso(Curso curso) {
		
		this.cursos.add(curso);
	}
	
	public void listarCursos() {
		
		System.out.println("Listado de cursos inscriptos\n");
		
		for(Curso cur: this.cursos) {
			
			System.out.println("Codigo:"+cur.getCodigo());
			System.out.println("Nombre:"+cur.getNombre());
			System.out.println("Profesor:"+cur.getProfesor().getNombre());
		}
	}
	
	
	

}
