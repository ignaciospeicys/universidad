package com.captton.programa;

import com.captton.universidad.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Alumno al1 = new Alumno(40640434,"Nacho","Speicys");
		Alumno al2 = new Alumno(40640435,"Matias","Michen");
		
		AlumnoPos alp = new AlumnoPos(30303030,"Franco","Lampard");
		
		Curso cur1 = new Curso(12,73222,"Analisis 1");
		Curso cur2 = new Curso(8, 22332, "Fisica 2");
		
		Profesor prof1 = new Profesor(36332111,"Juan","Kaka");
		Profesor prof2 = new Profesor(40640422,"Pepe","Salamin");
		
		cur1.setProfesor(prof1);
		cur2.setProfesor(prof2);
		
		
		System.out.println(cur1.getNombre() + " tiene como prof a "+cur1.getProfesor().getNombre());
		System.out.println(cur2.getNombre() + " tiene como prof a "+cur2.getProfesor().getNombre());
		
		al1.inscribirCurso(cur1);
		al1.inscribirCurso(cur2);
		
		al1.listarCursos();
		
		Universidad uni1 = new Universidad("UBA", "Ingenieria");
		
		uni1.inscribirAlumno(al1);
		uni1.inscribirAlumno(al2);
		
		uni1.buscarXdni(40640434);
		
		alp.setCuotaMensual(20000);
		
		System.out.println("\nCuota del alumno posgrado: "+alp.getCuotaMensual());
		
		
		
		

	}

}
